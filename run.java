class Buffer {
    private volatile int store;

    public synchronized void insert() {
        try{
            while(store == 5){ // Maximum limit of pots on the shelf.
                System.out.println("Shelf is full (Waiting...)");
                wait(); 
            } catch(InterruptedException e) {}

            store += 1; // Puts a pot on the shelf
            System.out.println("Shelf inserted a pot and now has " + store + " pots");

            notify();
        }
        public synchronized void remove() {
            try{
                while(store == 0){ // Indicates empty the shelf.
                    System.out.println("Shelf is empty (Waiting...)");
                    wait();
            } catch(InterruptedException e) {}
                store -= 1; // Removes a pot from the shelf
                System.out.println("Shelf removed a pot and now has " + store + " pots");
                notify();
            }
        }
    }
}

class Producer extends Thread {
    private Buffer buff;
    private String nameProducer;
    private int delay;

    public Producer(Buffer b, String n, int d) {
        buff = b;
        nameProducer = n;
        delay = d;
    }

    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                System.out.println(nameProducer + " is making a pot");
                sleep(delay); // Simulates time passing.
                buff.insert(); // Inserts a "pot" on the shelf.
                System.out.println(nameProducer + " has put a pot on the shelf");
            }
        } catch (InterruptedException e) {
        }
    }
}

class Packer extends Thread {
    private Buffer buff;
    private String namePacker;
    private int delay;

    public Packer(Buffer b, String n, int d) {
        buff = b;
        namePacker = n;
        delay = d;
    }

    public void run() {
        try {
            int packed = 0; // Counts how many pots were packed.
            for (int i = 0; i < 20; i++) {
                System.out.println(namePacker + " is ready to pack");
                sleep(delay); // Simulates time passing.
                buff.remove(); // Removes a "pot" from the shelf.
                ++packed;
                if (packed > 0) {
                    System.out.println(namePacker + " has packed pot " + packed);
                }
            }
        } catch (InterruptedException e) {
        }
    }
}

public class runner {
    private String name;
    private int delay;

    public runner(String name, int delay) {
        this.name = name;
        this.delay = delay;
    }

    public void start() {
        System.out.println(name + " has started");
    }

    public void startProduce(Buffer storage) {
        Producer pro = new Producer(storage, name, delay);
        pro.start();
    }

    public void startPacker(Buffer storage) {
        Packer con = new Packer(storage, name, delay);
        con.start();
    }

    public static void main(String[] args) {
        runner potterA = new runner("Ginny", 500); // runner(name, delay(minutes))
        runner potterB = new runner("Harry", 600); // runner(name, delay(minutes))
        runner potterC = new runner("Albus", 400); // runner(name, delay(minutes))
        Buffer shelf = new Buffer();

        potterA.start();
        potterB.start();
        packer.start();
        potterA.startProduce(shelf);
        potterB.startProduce(shelf);
        packer.startPacker(shelf);
    }
}